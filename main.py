from requests import post
import os


# Change the string below to change the output folder
# Make sure it ends with the double backslash!
output_dir: str = "C:\\Users\\jjask\\Desktop\\"

username: str = "mikegerson1"
company: str = "80513"
password: str = "Coaster1"
url: str = "https://api.faxage.com/httpsfax.php"


getfax = {
    "username": username,
    "company": company,
    "password": password,
    "operation": "getfax",
    "faxid": ""  # This is populated after the list of faxes is obtained
}
listfax = {
    "username": username,
    "company": company,
    "password": password,
    "operation": "listfax"
}
listfax_result = post(url, data=listfax)

# Make sure the request was successful
if listfax_result.status_code != 200:
    print(listfax_result.text)
    exit(-1)

for fax_str in listfax_result.text.split("\n"):
    if len(fax_str) > 0:
        # Extract the metadata from the response
        fax_id, timestamp, sender, receiver = fax_str.split("\t")
        # Format the timestamp and sender number so it can be used in a filename
        timestamp: str = timestamp.replace(':', '-').replace(' ', '_')
        sender: str = sender.replace('(', '').replace(')', '').replace(' ', '').replace('-', '')
        getfax["faxid"] = fax_id
        fax_data = post(url, data=getfax)
        if fax_data.headers["content-type"] == "application/octet-stream":
            # Contents of response are binary, save the contents to a file
            path: str = os.path.join(output_dir, f"{timestamp}_{sender}.pdf")
            file = open(path, 'wb')
            file.write(fax_data.content)
            print()
        else:
            print(fax_data.text)
